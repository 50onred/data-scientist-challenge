Use the supplied files, "training-data-set" and "training-data-labels", to build a model that as accurately as possible predicts the probability that a future example will have a label of 1. Use this model to predict the probability of a label of 1 for each example contained in the file "test-data-set". Along with your code, submit a file that has one predicted probability per line (that is, one prediction for each example in "test-data-set") and a header row, formatted in the same way as the file "sample-submission". Note that the variables cat1-7 are categorical, whereas the variables num1-3 are numerical.

Your submission will be evaluated based on the ROC area under the curve (AUC) that it produces on the test data set. Most statistical packages will have a library for calculating AUC -- you are welcome to use the R code below.

We are looking for

1. command of a language for data manipulation
2. basic proficiency in building a predictive model
3. basic understanding of the issues involved in model fitting

Please feel free to submit a basic predictive model. It is unnecessary to spend copious amounts of time improving AUC, although your submission should perform on par with a very basic benchmark model we have created.

    #
    # Prints AUC
    # 
    # preds - numerical predictions of class labels
    # labels - actual class labels
    #
    
    evalPerformance <- function(preds, labels) {
    
      if (!require("ROCR")) {
        install.packages("ROCR", repos="http://cran.rstudio.com/") 
        library("ROCR")
      }
      
      print("AUC: ")
      print(auc <- round(unlist(slot(performance(prediction(preds, labels), 'auc'), 'y.values')), 4))
    }
